<?php

namespace App\Services;


class Utils
{
    public static function getRemoteFileSize($url)
    {
        $regex = '/^Content-Length: *+\K\d++$/im';

        if (!$fp = @fopen($url, 'rb')) {
            return false;
        }

        if (
            isset($http_response_header) &&
            preg_match($regex, implode("\n", $http_response_header), $matches)
        ) {
            $bytes = (int)$matches[0];
        }

        $bytes = strlen(stream_get_contents($fp));

        if ($bytes < 1024) {
            return $bytes . ' Bytes';
        } elseif ($bytes >= 1024 && $bytes < 1048576) {
            return round($bytes / 1024, 2) . ' KB';
        }
        return round($bytes / 1048576, 2) . ' MB';
    }
}
