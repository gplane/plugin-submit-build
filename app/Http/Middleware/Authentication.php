<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\UserRepository;

class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!(new UserRepository)->has(session('uid'))) {
            $request->session()->clear();
            return redirect('/auth/login');
        }
        if (!$request->session()->get('logon')) {
            return redirect('/auth/login');
        }
        return $next($request);
    }
}
