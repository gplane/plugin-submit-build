<?php

namespace App\Http\Controllers;

use Hash;
use App\Models\User;
use App\Models\Plugin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $users_count = User::all()->count();
        $plugins_count = Plugin::all()->count();

        return view('admin.dashboard', ['users_count' => $users_count, 'plugins_count' => $plugins_count]);
    }

    public function usersPage()
    {
        $users = User::all();

        $users_list = ['data' => []];
        foreach ($users as $user) {
            $users_list['data'][] = [
                'uid' => $user->uid,
                'username' => $user->username,
                'email' => $user->email,
                'permission' => $user->permission,
                'register' => $user->created_at->__toString(),
                'operations' => '<a class="btn btn-success" onclick="editUser(' . $user->uid . ')">编辑</a> ' .
                    '<a class="btn btn-danger" onclick="deleteUser(' . $user->uid . ');">删除</a>'
            ];
        }

        return response()->json($users_list);
    }

    public function getUser(Request $request)
    {
        $user = User::find($request->input('uid'));

        $result = ['user' => ['uid' => $user->uid, 'username' => $user->username, 'email' => $user->email, 'permission' => $user->permission]];

        return response()->json($result);
    }

    public function updateUser(Request $request)
    {
        $post_user = $request->input('user');

        $user = User::find($post_user['uid']);

        $user->username = $post_user['username'];
        $user->email = $post_user['email'];
        $user->permission = $post_user['permission'];
        if (!empty($post_user['password']))
            $user->password = Hash::make($post_user['password']);

        $user->save();

        return response()->json(['code' => 0, 'msg' => '用户信息修改成功']);
    }

    public function deleteUser(Request $request)
    {
        $user = User::find($request->input('uid'));

        $user->delete();

        return response()->json(['code' => 0, 'msg' => '删除用户成功']);
    }

    public function pluginsPage()
    {
        $plugins = Plugin::all();

        $plugins_list = ['data' => []];
        foreach ($plugins as $plugin) {
            $plugins_list['data'][] = [
                'pid' => $plugin->pid,
                'title' => $plugin->title,
                'author' => $plugin->author,
                'description' => $plugin->description,
                'version' => $plugin->version,
                'size' => $plugin->size,
                'is_preview' => $plugin->is_preview == 1 ? true : false,
                'operations' => '<a class="btn btn-success" onclick="editPlugin(' . $plugin->pid . ')">编辑</a> ' .
                    '<a class="btn btn-danger" onclick="deletePlugin(' . $plugin->pid . ');">删除</a>'
            ];
        }

        return response()->json($plugins_list);
    }

    public function getPlugin(Request $request)
    {
        $plugin = Plugin::find($request->input('pid'));

        $result = ['plugin' => $plugin->makeHidden(['author', 'size', 'created_at', 'updated_at'])->toArray()];

        return response()->json($result);
    }

    public function updatePlugin(Request $request)
    {
        $post_plugin = $request->input('plugin');

        $plugin = Plugin::find($post_plugin['pid']);

        $plugin->name = $post_plugin['name'];
        $plugin->title = $post_plugin['title'];
        $plugin->uid = $post_plugin['uid'];
        $plugin->description = $post_plugin['description'];
        $plugin->version = $post_plugin['version'];
        $plugin->brief = $post_plugin['brief'];
        $plugin->url = $post_plugin['url'];
        $plugin->is_preview = $post_plugin['is_preview'] == 'true' ? 1 : 0;

        $plugin->save();

        return response()->json(['code' => 0, 'msg' => '插件信息修改成功']);
    }

    public function deletePlugin(Request $request)
    {
        $user = Plugin::find($request->input('pid'));

        $user->delete();

        return response()->json(['code' => 0, 'msg' => '删除插件成功']);
    }
}
