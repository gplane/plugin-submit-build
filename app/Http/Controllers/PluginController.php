<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Plugin;
use App\Services\Utils;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class PluginController extends Controller
{
    public function outputJSON()
    {
        $plugins = Plugin::all();

        $plugin_list = [];

        foreach ($plugins as $plugin) {
            $plugin_list[$plugin->name] = [
                'name' => $plugin->name,
                'title' => $plugin->title,
                'author' => $plugin->author,
                'description' => $plugin->description,
                'version' => $plugin->version,
                'size' => $plugin->size,
                'brief' => $plugin->brief,
                'url' => $plugin->url,
                'isPreview' => $plugin->is_preview == 1 ? true : false
            ];
        }

        return response()->json($plugin_list);
    }

    public function add(Request $request, UserRepository $users)
    {
        $user = $users->getCurrentUser();

        //Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'url' => 'required|url'
        ]);
        if ($validator->fails())
            return response()->json(['code' => 2, 'msg' => '要认认真真填写哦']);

        //Get file size
        $size = Utils::getRemoteFileSize($request->url);
        if (!$size) {
            return response()->json(['code' => 2, 'msg' => '您所提交的插件下载 URL 不是有效的']);
        }

        $plugin = new Plugin;

        $plugin->name = $request->name;
        $plugin->title = $request->title;
        $plugin->author = $user->username;
        $plugin->uid = $user->uid;
        $plugin->description = $request->has('description') ? $request->description : '';
        $plugin->version = $request->has('version') ? $request->version : '';
        $plugin->size = $size;
        $plugin->brief = $request->has('brief') ? $request->brief : '';
        $plugin->url = $request->url;
        $plugin->is_preview = $request->has('is_preview') ? $request->is_preview : 0;

        $plugin->save();

        return response()->json(['code' => 0, 'msg' => '提交成功']);
    }

    public function update(Request $request)
    {
        //Validation
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'title' => 'required',
            'url' => 'required|url'
        ]);
        if ($validator->fails())
            return response()->json(['code' => 2, 'msg' => '要认认真真填写哦']);

        //Get file size
        $size = Utils::getRemoteFileSize($request->url);
        if (!$size) {
            return response()->json(['code' => 2, 'msg' => '您所提交的插件下载 URL 不是有效的']);
        }

        $plugin = Plugin::find($request->session()->get('editing_pid'))->first();

        $plugin->name = $request->name;
        $plugin->title = $request->title;
        $plugin->description = $request->has('description') ? $request->description : '';
        $plugin->version = $request->has('version') ? $request->version : '';
        $plugin->size = $size;
        $plugin->brief = $request->has('brief') ? $request->brief : '';
        $plugin->url = $request->url;
        $plugin->is_preview = $request->has('is_preview') ? $request->is_preview : 0;

        $plugin->save();

        return response()->json(['code' => 0, 'msg' => '提交成功']);
    }
}
