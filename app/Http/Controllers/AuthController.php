<?php

namespace App\Http\Controllers;

use Mail;
use Crypt;
use Validator;
use App\Models\User;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function showLoginPage(Request $request)
    {
        //Check if user remember log status
        if ($request->cookie('remember')) {
            $request->session()->regenerate();
            $request->session()->put('logon', true);
            return redirect('/home');
        }

        if ($request->session()->get('logon'))
            return redirect('/home');

        return view('auth.login');
    }

    public function login(Request $request)
    {
        //Check if user has logon
        if ($request->session()->get('logon')) {
            return response()->json(['code' => 1, 'msg' => '您已登录']);
        }

        //Check if values exist
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'type' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Start to login
        $user = new User;
        switch ($request->input('type')) {
            case 'username':
                $user = User::where('username', $request->input('username'))->first();
                break;
            case 'email':
                $user = User::where('email', $request->input('username'))->first();
                break;
        }
        if (!$user) {
            return response()->json(['code' => 1, 'msg' => '请先注册']);
        } else {
            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['code' => 2, 'msg' => '密码不对']);
            }
            $request->session()->regenerate();
            $request->session()->put('uid', $user->uid);
            $request->session()->put('logon', true);
            return response()->json(['code' => 0, 'msg' => '登录成功'])->cookie('remember', $request->input('remember'));
        }
    }

    public function logout(Request $request)
    {
        if ($request->session()->get('logon')) {
            $request->session()->clear();
            return response()->json(['code' => 0, 'msg' => '注销成功'])->cookie('remember', false);
        }
        return response()->json(['code' => 1, 'msg' => '还没登录呢']);
    }

    public function register(Request $request)
    {
        //Validate if user exists
        $user = User::where('username', $request->input('username'))->first();
        if ($user) {
            return response()->json(['code' => -1, 'msg' => '您已经注册了哦']);
        }

        //Validate if user exists
        $user = User::where('email', $request->input('email'))->first();
        if ($user) {
            return response()->json(['code' => -1, 'msg' => '您已经注册了哦']);
        }

        //Validate data
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:20',
            'email' => 'required|email',
            'password' => 'required|min:8|max:30',
            'confirm' => 'required|same:password'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 2, 'msg' => '要认认真真填写哦']);
        }

        //Save information to database
        $user = new User;
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->permission = 0;
        $user->save();

        $request->session()->regenerate();
        $request->session()->put('uid', $user->uid);
        $request->session()->put('logon', true);

        return response()->json(['code' => 0, 'msg' => '注册成功']);
    }

    public function forgot(Request $request)
    {
        //Validate data
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Check if user exists
        $user = User::where('username', $request->input('username'))->first();
        if (!$user)
            return response()->json(['code' => 1, 'msg' => '确定已经注册了？']);

        //Validate email
        if ($user->email != $request->input('email'))
            return response()->json(['code' => 2, 'msg' => '邮箱不对']);

        //Generate unique link
        $key = str_random(24);
        $user->password = $key;
        $user->save();

        $link = url('/auth/reset') . '?uid=' . $user->uid . '&key=' . Crypt::encrypt($key);

        //Send email
        Mail::to($user->email)->send(new ResetPassword($link));

        return response()->json(['code' => 0, 'msg' => '重置密码的链接已通过邮件发送到您的邮箱']);
    }

    public function reset(Request $request)
    {
        //Prevent from empty info
        if (!$request->has('uid') || !$request->has('key')) {
            return response()->make(['code' => 1, 'msg' => '如果您没想要重置密码，就不要来搞啦']);
        }

        //Gather necessary information
        $uid = $request->input('uid');
        $key = Crypt::decrypt($request->input('key'));

        //Validation
        $user = User::find($uid);
        if ($user->password != $key) {
            return response()->json(['code' => 2, 'msg' => '重置密码信息有误']);
        }

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|max:30',
            'confirm' => 'required|same:password'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 2, 'msg' => '密码不符合要求']);
        }

        //Start to reset password
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return response()->json(['code' => 0, 'msg' => '好了，您可以继续使用这个账号了']);
    }
}
