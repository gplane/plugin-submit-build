<?php

namespace App\Http\Controllers;

use Hash;
use Schema;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;

class SetupController extends Controller
{
    public function install(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要把信息填好哦']);
        }

        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->tinyInteger('permission');
            $table->timestampsTz();
        });

        Schema::create('plugins', function (Blueprint $table) {
            $table->increments('pid');
            $table->string('name');
            $table->string('author');
            $table->integer('uid');
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('size')->nullable();
            $table->string('version')->nullable();
            $table->string('brief')->nullable();
            $table->string('url');
            $table->tinyInteger('is_preview');
            $table->timestampsTz();
        });

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->permission = 1;
        $user->save();

        $request->session()->push('logon', true);
        $request->session()->push('uid', 1);

        return response()->json(['code' => 0, 'msg' => '安装成功']);
    }
}
