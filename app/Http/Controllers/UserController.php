<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Plugin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    public function renewPassword(Request $request, UserRepository $users)
    {
        //Validate
        $validator = Validator::make($request->all(), [
            'old' => 'required',
            'new' => 'required|min:8|max:30',
            'confirm' => 'required|same:new'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Validate old password
        $user = $users->getCurrentUser();
        if (!Hash::check($request->input('old'), $user->password)) {
            return response()->json(['code' => 2, 'msg' => '旧密码不对']);
        }

        //Save new password
        $user->password = Hash::make($request->input('new'));
        $user->save();
        return response()->json(['code' => 0, 'msg' => '修改密码成功']);
    }

    public function renewEmail(Request $request, UserRepository $users)
    {
        //Validate
        $validator = Validator::make($request->all(), [
            'old' => 'required|email',
            'new' => 'required|email',
            'confirm' => 'required|same:new'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Validate old email
        $user = $users->getCurrentUser();
        if ($request->input('old') != $user->email) {
            return response()->json(['code' => 2, 'msg' => '旧邮箱不对']);
        }

        //Save new email
        $user->email = $request->input('new');
        $user->save();
        return response()->json(['code' => 0, 'msg' => '修改邮箱成功']);
    }

    public function renewUsername(Request $request, UserRepository $users)
    {
        //Validate
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:20'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Save new username
        $user = $users->getCurrentUser();
        $user->username = $request->input('username');
        $user->save();

        //Update current plugins author
        $plugins = Plugin::where('uid', $user->uid)->get();
        foreach ($plugins as $plugin) {
            $plugin->author = $user->username;
            $plugin->save();
        }

        return response()->json(['code' => 0, 'msg' => '修改用户名成功']);
    }

    public function deleteUser(Request $request, UserRepository $users)
    {
        //Validate
        $validator = Validator::make($request->all(), [
            'username' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '要认认真真填写哦']);
        }

        //Validate username
        $user = $users->getCurrentUser();
        if ($user->username != $request->input('username')) {
            return response()->json(['code' => 2, 'msg' => '用户名不对']);
        }

        //Delete user
        $user->delete();

        //Clear session and cookies
        $request->session()->clear();
        $request->session()->regenerate();
        return response()->json(['code' => 0, 'msg' => '账号删除成功，再见'])->cookie('remember', false);
    }
}
