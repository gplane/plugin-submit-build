<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Plugin;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;

class HomeController extends Controller
{
    public function dashboard(Request $request, UserRepository $users)
    {
        $user = $users->getCurrentUser();
        $plugin_count = Plugin::where('uid', $request->session()->get('uid'))->count();
        return view('home.dashboard', ['user' => $user, 'plugin_count' => $plugin_count]);
    }

    public function manage(UserRepository $users)
    {
        $user = $users->getCurrentUser();
        $plugins = Plugin::where('uid', $user->uid)->get();

        $plugin_list = ['data' => []];

        foreach ($plugins as $plugin) {
            $plugin_list['data'][] = [
                'title' => $plugin->title,
                'description' => $plugin->description,
                'version' => $plugin->version,
                'size' => $plugin->size,
                'brief' => '<a href="' . $plugin->brief . '" target="_blank" class="btn btn-link">' . substr($plugin->brief, 0, 10) . ' ...</a>',
                'manage' => '<a class="btn btn-success" href="/home/manage/plugin?name=' . $plugin->name . '">编辑</a> ' .
                    '<a class="btn btn-danger" onclick="deletePlugin(' . $plugin->pid . ');">删除</a>'
            ];
        }

        return response()->json($plugin_list);
    }

    public function managePlugin(Request $request, UserRepository $users)
    {
        //Check plugin name if is empty
        if (!$request->has('name')) {
            return response()->json(['code' => 1, 'msg' => '您还没选择一个插件呢']);
        }

        //Get uid
        $user = $users->getCurrentUser();

        $plugin = Plugin::where('name', $request->input('name'))->first();

        //Prevent modifying others' plugin
        if ($plugin->uid != $user->uid) {
            return response()->json(['code' => 2, 'msg' => '这个插件不是您制作的哦']);
        }

        request()->session()->push('editing_pid', $plugin->pid);
        return view('home.add', ['title' => '编辑插件信息', 'plugin' => $plugin, 'method' => 'editPlugin()']);
    }

    public function add()
    {
        return view('home.add', ['title' => '提交新插件', 'plugin' => new Plugin, 'method' => 'addPlugin()']);
    }

    public function remove(Request $request, UserRepository $users)
    {
        $validator = Validator::make($request->all(), [
            'pid' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return response()->json(['code' => 1, 'msg' => '您未选择要删除的插件']);
        }

        $user = $users->getCurrentUser();

        $plugin = Plugin::find($request->input('pid'))->first();

        //Prevent modifying others' plugin
        if ($plugin->uid != $user->uid) {
            return response()->json(['code' => 2, 'msg' => '这个插件不是您制作的哦']);
        }

        $plugin->delete();

        return response()->json(['code' => 0, 'msg' => '删除成功']);
    }
}
