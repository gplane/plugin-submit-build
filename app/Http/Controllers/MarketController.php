<?php

namespace App\Http\Controllers;

use App\Models\Plugin;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    public function market()
    {
        $plugins = Plugin::all();

        return $plugins->makeHidden(['name', 'uid', 'size', 'version', 'brief', 'url', 'is_preview', 'created_at'])->toArray();
    }
}
