<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    public $primaryKey = 'pid';

    protected $fillable = ['name', 'author', 'uid', 'title', 'description', 'size', 'version', 'brief', 'url', 'is_preview'];
}
