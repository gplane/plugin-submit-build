<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    public function get($uid)
    {
        return User::find($uid);
    }

    public function getCurrentUser()
    {
        return $this->get(session('uid'));
    }

    public function has($uid)
    {
        $user = User::where('uid', $uid);
        return $user->count() == 1;
    }
}
