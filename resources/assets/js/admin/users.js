"use strict";
var table = $('#users-list').DataTable({
    language: dataTableLanguage,
    responsive: true,
    autoWidth: false,
    processing: true,
    ajax: '/admin/users/list',
    createRow: function (row, data, index) {
    },
    columns: [
        {data: 'uid'},
        {data: 'username'},
        {data: 'email'},
        {data: 'permission'},
        {data: 'register'},
        {data: 'operations', searchable: false, orderable: false}
    ]
});

var userEdit = new Vue({
    el: '#user-edit',
    data: {
        user: {
            uid: 0,
            username: '',
            email: '',
            password: '',
            permission: ''
        }
    }
});

function editUser(uid) {
    $.get(
        '/admin/users/get',
        {uid: uid},
        function (result) {
            userEdit.user = result.user;
        }
    );
    $('#user-edit').removeClass('hidden').removeClass('fadeOutUp').addClass('fadeInDown');
}

function updateUser() {
    $.post(
        '/admin/users/post',
        {
            _token: $('[name=_token]').val(),
            user: userEdit.user
        },
        function (result) {
            showMessage(result, '');
            $('#user-edit').removeClass('fadeInDown').addClass('fadeOutUp');
            window.setTimeout('$("#user-edit").addClass("hidden")', 750);
            table.ajax.reload(false, null);
        }
    );
}

function cancelEditUser() {
    $('#user-edit').removeClass('fadeInDown').addClass('fadeOutUp');
    window.setTimeout('$("#user-edit").addClass("hidden")', 750);
}

function deleteUser(uid) {
    swal({
        title: '前方小心',
        text: '删除用户的操作不可恢复，继续吗？',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '是的，我要删除',
        cancelButtonText: '别，我只是点错了',
        animation: false,
        customClass: 'animated shake'
    }).then(function () {
        $.post(
            '/admin/users/delete',
            {
                uid: uid,
                _token: $('[name=_token]').val()
            },
            function (result) {
                showMessage(result, '');
                table.ajax.reload(false, null);
            }
        );
    });
}
