"use strict";
var pluginTable = $('#plugins-list').DataTable({
    language: dataTableLanguage,
    responsive: true,
    autoWidth: false,
    processing: true,
    ajax: '/admin/plugins/list',
    createRow: function (row, data, index) {
    },
    columns: [
        {data: 'pid'},
        {data: 'title'},
        {data: 'description'},
        {data: 'author'},
        {data: 'size'},
        {data: 'version'},
        {data: 'is_preview'},
        {data: 'operations', searchable: false, orderable: false}
    ]
});

var pluginEdit = new Vue({
    el: '#plugin-edit',
    data: {
        plugin: {
            pid: 0,
            name: '',
            title: '',
            uid: '',
            description: '',
            version: '',
            brief: '',
            url: '',
            is_preview: false
        }
    }
});

function editPlugin(pid) {
    $.get(
        '/admin/plugins/get',
        {pid: pid},
        function (result) {
            pluginEdit.plugin = result.plugin;
        }
    );
    $('#plugin-edit').removeClass('hidden').removeClass('fadeOutUp').addClass('fadeInDown');
}

function updatePlugin() {
    $.post(
        '/admin/plugins/post',
        {
            _token: $('[name=_token]').val(),
            plugin: pluginEdit.plugin
        },
        function (result) {
            showMessage(result, '');
            $('#plugin-edit').removeClass('fadeInDown').addClass('fadeOutUp');
            window.setTimeout('$("#plugin-edit").addClass("hidden")', 750);
            pluginTable.ajax.reload(false, null);
        }
    );
}

function cancelEditPlugin() {
    $('#plugin-edit').removeClass('fadeInDown').addClass('fadeOutUp');
    window.setTimeout('$("#plugin-edit").addClass("hidden")', 750);
}

function deletePlugin(pid) {
    swal({
        title: '前方小心',
        text: '删除插件的操作不可恢复，继续吗？',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '是的，我要删除',
        cancelButtonText: '别，我只是点错了',
        animation: false,
        customClass: 'animated shake'
    }).then(function () {
        $.post(
            '/admin/plugins/delete',
            {
                pid: pid,
                _token: $('[name=_token]').val()
            },
            function (result) {
                showMessage(result, '');
                pluginTable.ajax.reload(false, null);
            }
        );
    });
}
