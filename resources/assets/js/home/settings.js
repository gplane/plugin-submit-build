"use strict";
function changePassword() {
    $.post(
        '/user/renew/password',
        {
            old: $('#old-password').val(),
            new:$('#new-password').val(),
            confirm:$('#confirm-password').val(),
            _token:$('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '/');
        }
    );
}

function changeEmail() {
    $.post(
        '/user/renew/email',
        {
            old: $('#old-email').val(),
            new:$('#new-email').val(),
            confirm:$('#confirm-email').val(),
            _token:$('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '/');
        }
    );
}

function changeUsername() {
    $.post(
        '/user/renew/username',
        {
            username: $('#author-name').val(),
            _token:$('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '/');
        }
    );
}

function deleteUser() {
    $.post(
        '/user/delete',
        {
            username: $('#delete-confirm').val(),
            _token:$('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '/');
        }
    );
}
