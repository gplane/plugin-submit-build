"use strict";
function addPlugin() {
    $.post(
        '/plugin/add',
        {
            name: $('#name').val(),
            title: $('#title').val(),
            description: $('#description').val(),
            version: $('#version').val(),
            is_preview: $('#is_preview').prop('checked') ? 1 : 0,
            url: $('#url').val(),
            brief: $('#brief').val(),
            _token: $('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '/home/manage');
        }
    );
}
