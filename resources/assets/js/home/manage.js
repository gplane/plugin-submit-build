$('#personal-plugin-list').DataTable({
    language: dataTableLanguage,
    responsive: true,
    autoWidth: false,
    processing: true,
    ajax: '/home/manage/list',
    createRow: function (row, data, index) {
    },
    columns: [
        {data: 'title'},
        {data: 'description', 'width': '35%'},
        {data: 'size'},
        {data: 'version'},
        {data: 'brief'},
        {data: 'manage', searchable: false, orderable: false}
    ]
});

function deletePlugin(pid) {
    swal({
        title: '前方小心',
        text: '删除插件的操作不可恢复，继续吗？',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '是的，我要删除',
        cancelButtonText: '别，我只是点错了',
        animation: false,
        customClass: 'animated shake'
    }).then(function () {
        $.get(
            '/home/manage/plugin/remove',
            {
                pid: pid
            },
            function (result) {
                showMessage(result, '/home/manage');
            }
        );
    });

}
