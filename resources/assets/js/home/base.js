"use strict";
function logout() {
    $.post(
        '/auth/logout',
        { _token: $('[name=_token]').val()},
        function (result) {
            showMessage(result, '/');
        }
    );
}
