"use strict";
function register() {
    $.post(
        '/auth/register/handle',
        {
            username: $('#username').val(),
            email: $('#email').val(),
            password: $('#password').val(),
            confirm: $('#confirm').val(),
            _token: $('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '../home');
        });
}
