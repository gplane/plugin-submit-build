"use strict";
function forgotPassword() {
    $.post(
        '/auth/forgot/handle',
        {
            username: $('#username').val(),
            email: $('#email').val(),
            _token: $('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '');
        });
}
