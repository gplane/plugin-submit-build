"use strict";
function login() {
    var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    var usernameType = 'email';
    if ($('#username').val().search(pattern) == -1) {
        usernameType = 'username';
    }
    $.post(
        '/auth/login',
        {
            username: $('#username').val(),
            password: $('#password').val(),
            type: usernameType,
            remember: $('#remember').prop('checked'),
            _token: $('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '../home');
        });
}
