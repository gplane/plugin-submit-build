"use strict";
function resetPassword() {
    var pattern = /\?uid=(\d+)\&key=(\w+)/;
    var result = window.location.search.match(pattern);
    var uid = result[1];
    var key = result[2];
    $.post(
        '/auth/reset/handle',
        {
            password: $('#password').val(),
            confirm: $('#confirm').val(),
            uid: uid,
            key: key,
            _token: $('[name=_token]').val()
        },
        function (result) {
            showMessage(result, '../');
        });
}
