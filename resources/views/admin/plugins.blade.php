@extends('admin.base')

@section('title', '插件管理')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">插件管理</h1>
        </div>
    </div>

    <div class="row animated hidden" id="plugin-edit">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <input type="hidden" v-model="plugin.pid">
                <div class="panel-heading">插件信息编辑</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>name 值：</label>
                                <input type="text" class="form-control" v-model="plugin.name">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>名称：</label>
                                <input type="text" class="form-control" v-model="plugin.title">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>作者 UID：</label>
                                <input type="text" class="form-control" v-model="plugin.uid">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>下载地址：</label>
                                <input type="text" class="form-control" v-model="plugin.url">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>简介页 URL：</label>
                                <input type="text" class="form-control" v-model="plugin.brief">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label>版本：</label>
                                <input type="text" class="form-control" v-model="plugin.version">
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <div class="form-group">
                                <label>预览版：</label>
                                <div><input type="checkbox" v-model="plugin.is_preview">是预览版</div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>描述：</label>
                                <textarea class="form-control" v-model="plugin.description"></textarea>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning" onclick="updatePlugin()">提交</button>
                    <button class="btn btn-warning" onclick="cancelEditPlugin()">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">插件列表</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="plugins-list">
                        <thead>
                        <tr>
                            <th>PID</th>
                            <th>名称</th>
                            <th>描述</th>
                            <th>作者</th>
                            <th>大小</th>
                            <th>版本</th>
                            <th>是否预览版</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
