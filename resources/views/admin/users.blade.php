@extends('admin.base')

@section('title', '用户管理')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">用户管理</h1>
        </div>
    </div>

    <div class="row animated hidden" id="user-edit">
        <div class="col-lg-12">
            <div class="panel panel-warning">
                <input type="hidden" v-model="user.uid">
                <div class="panel-heading">用户信息编辑</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>用户名：</label>
                                <input type="text" class="form-control" v-model="user.username">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>邮箱：</label>
                                <input type="text" class="form-control" v-model="user.email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>密码：</label>
                                <input type="text" class="form-control" v-model="user.password" placeholder="留空表示保持默认">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>权限：</label>
                                <input type="text" class="form-control" v-model="user.permission">
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-warning" onclick="updateUser()">提交</button>
                    <button class="btn btn-warning" onclick="cancelEditUser()">取消</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">用户列表</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="users-list">
                        <thead>
                        <tr>
                            <th>UID</th>
                            <th>用户名</th>
                            <th>Email</th>
                            <th>权限</th>
                            <th>注册时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
