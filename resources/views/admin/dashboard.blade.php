@extends('admin.base')

@section('title', '仪表盘')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">仪表盘</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $users_count }}</div>
                            <div>用户数量</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/users">
                    <div class="panel-footer">
                        <span class="pull-left">详细</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-plug fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $plugins_count }}</div>
                            <div>插件数量</div>
                        </div>
                    </div>
                </div>
                <a href="/admin/plugins">
                    <div class="panel-footer">
                        <span class="pull-left">详细</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection

