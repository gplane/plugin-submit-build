@extends('auth.base')

@section('title', '安装')

@section('subtitle', '欢迎安装')

@section('form')
    <div class='form-group'>
        <input class='form-control' placeholder='管理员用户名' type='text' id="admin-username">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='管理员邮箱' type='text' id="admin-email">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='密码' type='password' id="admin-password">
    </div>
    <div class='text-center'>
        <a class="btn btn-default" onclick="install()">安装</a>
    </div>

    <script>
        function install() {
            $.post(
                '/setup/go',
                {
                    username: $('#admin-username').val(),
                    email: $('#admin-email').val(),
                    password: $('#admin-password').val(),
                    _token: $('[name=_token]').val()
                },
                function (result) {
                    showMessage(result, '../');
                }
            );
        }
    </script>
@endsection
