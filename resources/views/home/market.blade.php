@extends('home.base')

@section('title', '插件市场')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">插件市场</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-striped table-bordered table-hover" id="market-list">
                <tbody>
                <tr v-for="plugin in plugins">
                    <td>
                        <div class="row">
                            <div class="col-sm-11">
                                <h3>@{{ plugin.title }} <small> @{{ plugin.author }}</small></h3>
                            </div>
                            <div class="col-sm-1">
                                <a class="btn btn-warning">详细</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-10">
                                <span>@{{ plugin.description }}</span>
                            </div>
                            <div class="col-lg-1">
                                <i class="fa fa-comment fa-fw"></i>
                                <span> @{{ plugin.commentCount }}</span>
                            </div>
                            <div class="col-lg-1">
                                <a><i class="fa fa-heart fa-fw"></i> @{{ plugin.likedCount }}</a>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script>
        let market = new Vue({
            el: '#market-list',
            data: {
                plugins: []
            }
        });

        $('#market-list').dataTable({});

        $(document).ready(function () {
            $.get(
                '/home/market/list',
                function (result) {
                    market.plugins = result;
                }
            );
        });
    </script>
@endsection
