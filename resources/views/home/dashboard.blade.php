@extends('home.base')

@section('title', '仪表盘')

@section('content')
<div class="row">
    <div class="col-lg-6">
        <h1 class="page-header">仪表盘</h1>
    </div>
</div>

<div class="row">
    <div class="col-md-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-plug fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{ $plugin_count }}</div>
                        <div>您制作的插件数量</div>
                    </div>
                </div>
            </div>
            <a href="/home/manage">
                <div class="panel-footer">
                    <span class="pull-left">详细</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-bullhorn" aria-hidden="true"></i> 公告</div>
            <div class="panel-body">
                暂无
            </div>
        </div>
    </div>
</div>
@endsection
