@extends('home.base')

@section('title', '管理现有插件')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">管理现有插件</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-success">
                <div class="panel-heading">插件列表</div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="personal-plugin-list">
                        <thead>
                            <tr>
                                <th>名称</th>
                                <th>描述</th>
                                <th>大小</th>
                                <th>版本</th>
                                <th>简介页 URL</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
