@extends('home.base')

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ $title }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-success">
                <div class="panel-heading">插件基本信息填写</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>插件唯一的 name 值：</label>
                        <input type="text" id="name" class="form-control" value="{{ $plugin->name }}">
                    </div>
                    <div class="form-group">
                        <label>插件的名称（用于显示）：</label>
                        <input type="text" id="title" class="form-control" value="{{ $plugin->title }}">
                    </div>
                    <div class="form-group">
                        <label>描述：</label>
                        <textarea class="form-control" id="description" placeholder="不宜太长也不宜太短">{{ $plugin->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>插件版本：</label>
                        <input id="version" class="form-control" value="{{ $plugin->version }}">
                    </div>
                    <div class="form-group">
                        <label>版本：</label>
                        <label class="checkbox-inline"><input id="is_preview" type="checkbox"{{ $plugin->is_preview == 1 ? ' checked="checked"' : '' }}>这是预览版</label>
                    </div>
                    <div class="form-group">
                        <label>插件下载地址：</label>
                        <input class="form-control" id="url" value="{{ $plugin->url }}" placeholder="不能填百度网盘之类的，必须是能直接访问下载的">
                    </div>
                    <div class="form-group">
                        <label>插件详情页：</label>
                        <input type="text" id="brief" value="{{ $plugin->brief }}" placeholder="也可以填您的个人主页，或者不填，但如果填，就要写绝对路径" class="form-control">
                    </div>
                    <button class="btn btn-primary" onclick="{{ $method }}">好了</button>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="panel panel-warning">
                <div class="panel-heading">如何填写？</div>
                <div class="panel-body">还没写好</div>
            </div>
        </div>
    </div>
@endsection
