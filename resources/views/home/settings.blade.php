@extends('home.base')

@section('title', '账号设置')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                账号设置
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-asterisk" aria-hidden="true"></i> 修改密码</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>旧密码：</label>
                        <input class="form-control" type="password" id="old-password">
                    </div>
                    <div class="form-group">
                        <label>新密码：</label>
                        <input class="form-control" type="password" id="new-password">
                    </div>
                    <div class="form-group">
                        <label>再次确认：</label>
                        <input class="form-control" type="password" id="confirm-password">
                    </div>
                    <button onclick="changePassword()" class="btn btn-primary">提交</button>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading"><i class="fa fa-envelope" aria-hidden="true"></i> 修改邮箱</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>旧邮箱：</label>
                        <input class="form-control" type="text" id="old-email">
                    </div>
                    <div class="form-group">
                        <label>新邮箱：</label>
                        <input class="form-control" type="text" id="new-email">
                    </div>
                    <div class="form-group">
                        <label>再次确认：</label>
                        <input class="form-control" type="text" id="confirm-email">
                    </div>
                    <button onclick="changeEmail()" class="btn btn-primary">提交</button>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-yellow">
                <div class="panel-heading"><i class="fa fa-drivers-license" aria-hidden="true"></i> 更改用户名</div>
                <div class="panel-body">
                    注意：这会修改您现有所有插件的作者名
                    <div class="form-group">
                        <label>新的名称：</label>
                        <input type="text" class="form-control" id="author-name">
                    </div>
                    <button onclick="changeUsername()" class="btn btn-warning">提交</button>
                </div>
            </div>
            <div class="panel panel-red">
                <div class="panel-heading"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> 删除账号</div>
                <div class="panel-body">
                    <b>我们不生产后悔药，我们只是……也不是后悔药的搬运工！</b>
                    <div class="form-group">
                        <label>您的用户名：</label>
                        <input type="text" class="form-control" id="delete-confirm">
                    </div>
                    <button onclick="deleteUser()" class="btn btn-danger">提交</button>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">友情提示</div>
                <div class="panel-body">
                    不建议频繁修改个人账号信息
                </div>
            </div>
        </div>
    </div>
@endsection
