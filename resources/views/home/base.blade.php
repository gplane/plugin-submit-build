<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title') - 开发者中心 - 插件市场</title>

    <link href="{{ url('css/app.min.css') }}" rel="stylesheet">

</head>

<body>

<?php $user = \App\Models\User::find(session('uid'))->first(); ?>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><i class="fa fa-shopping-bag" aria-hidden="true"></i> 插件市场</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>
                    {{ $user->username }}
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="/home"><i class="fa fa-user fa-fw"></i> 开发者中心</a>
                    </li>
                    <li><a href="/home/settings"><i class="fa fa-gear fa-fw"></i> 账户设置</a>
                    </li>
                    <li class="divider"></li>
                    <li><a onclick="logout()"><i class="fa fa-sign-out fa-fw"></i> 注销</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="/home"><i class="fa fa-dashboard fa-fw"></i> 仪表盘</a>
                    </li>
                    <li>
                        <a href="/home/manage"><i class="fa fa-plug fa-fw"></i> 管理现有插件</a>
                    </li>
                    <li>
                        <a href="/home/add"><i class="fa fa-plus fa-fw"></i> 提交新插件</a>
                    </li>
                    <li>
                        <a href="/home/settings"><i class="fa fa-wrench fa-fw"></i> 账户设置</a>
                    </li>
                    <li>
                        <a href="/home/market"><i class="fa fa-paper-plane fa-fw"></i> 浏览市场</a>
                    </li>
                    <li>
                        <a href="/admin"><i class="fa fa-server fa-fw"></i> 管理中心</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        {{ csrf_field() }}
        @section('content')
        @show
    </div>
    <!-- /#page-wrapper -->

    <footer class="modal-footer">
        <div class="pull-left">
            登录、认证部分的模板修改自 <a href="https://github.com/lab2023/hierapolis" target="_blank">Hierapolis</a>，
            此页面使用 <a href="https://github.com/BlackrockDigital/startbootstrap-sb-admin-2" target="_blank">SB Admin 2</a>
            模板
        </div>
        <div class="pull-right hidden-xs">
            由 <a href="https://github.com/g-plane" target="_blank">GPlane</a> 制作了插件市场以及此系统
        </div>
    </footer>

</div>
<!-- /#wrapper -->

<script src="{{ url('js/app.min.js') }}"></script>
<script src="{{ url('js/sb-admin-2.min.js') }}"></script>
<script src="{{ url('js/home.min.js') }}"></script>
@section('script')
@show
</body>

</html>

