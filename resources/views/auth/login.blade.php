@extends('auth.base')

@section('title', '登录')

@section('subtitle', '登录您的账号')

@section('form')
    <div class='form-group'>
        <input class='form-control' placeholder='用户名或邮箱' type='text' id="username">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='密码' type='password' id="password">
    </div>
    <div class='text-center'>
        <div class='checkbox'>
            <label>
                <input type='checkbox' id="remember">
                记住我
            </label>
        </div>
        <a class="btn btn-default" onclick="login()">登录</a>
        <br>
        <a href="/auth/forgot">忘记密码？</a>
        <a href="/auth/register" style="margin-left: 26px">成为开发者</a>
    </div>
@endsection
