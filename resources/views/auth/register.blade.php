@extends('auth.base')

@section('title', '成为开发者')

@section('subtitle', '加入我们，成为开发者')

@section('form')
    <div class='form-group'>
        <input class='form-control' placeholder='用户名，最多20位' type='text' id="username">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='邮箱' type='text' id="email">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='密码，8至30位' type='password' id="password">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='确认密码' type='password' id="confirm">
    </div>
    <div class='text-center'>
        <a class="btn btn-default" onclick="register()">注册</a>
        <br>
        <a href="/">已有账号了？来这里登录</a>
    </div>
@endsection
