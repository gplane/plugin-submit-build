@extends('auth.base')

@section('title', '重置密码')

@section('subtitle', '重置密码')

@section('form')
    <div class='form-group'>
        <input class='form-control' placeholder='密码，8至30位' type='password' id="password">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='确认密码' type='password' id="confirm">
    </div>
    <div class='text-center'>
        <a class="btn btn-default" onclick="resetPassword()">提交</a>
    </div>
@endsection
