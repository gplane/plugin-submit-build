@extends('auth.base')

@section('title', '忘记密码')

@section('subtitle', '即将重置密码')

@section('form')
    <div class='form-group'>
        <input class='form-control' placeholder='用户名' type='text' id="username">
    </div>
    <div class='form-group'>
        <input class='form-control' placeholder='该账号所使用的邮箱' type='text' id="email">
    </div>
    <div class='text-center'>
        <a class="btn btn-default" onclick="forgotPassword()">提交</a>
        <br>
        <a href="/">蜜汁突然想起</a>
    </div>
@endsection
