<!DOCTYPE html>
<html class='no-js' lang='en'>
<head>
    <meta charset='utf-8'>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title') - 账号与安全 - 插件市场</title>
    <link href="{{ url('css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('css/auth.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class='login'>
<div class='wrapper'>
    <div class='row'>
        <div class='col-lg-12'>
            <div class='brand text-center'>
                <h1>
                    <div class='logo-icon'>
                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:#ecf0f1"></i>
                    </div>
                    插件市场
                </h1>
            </div>
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-12'>
            <form>
                {{ csrf_field() }}
                <fieldset class='text-center'>
                    <legend>@yield('subtitle')</legend>
                    @section('form')
                    @show
                </fieldset>
            </form>
        </div>
    </div>
</div>
<!-- Footer -->
<!-- Javascripts -->
<script src="{{ url('js/app.min.js') }}"></script>
<script src="{{ url('js/auth.min.js') }}"></script>
</body>
</html>
