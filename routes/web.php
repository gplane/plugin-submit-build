<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'setup', 'middleware' => 'web'], function () {
    Route::any('/', function () {
        return view('setup');
    });
    Route::post('/go', 'SetupController@install');
});

Route::get('/', 'AuthController@showLoginPage')->middleware('web');

Route::any('/plugin.json', 'PluginController@outputJSON');

Route::group(['prefix' => 'plugin', 'middleware' => ['web', 'auth']], function () {
    Route::post('/add', 'PluginController@add');
    Route::post('/update', 'PluginController@update');
});

Route::group(['prefix' => 'auth', 'middleware' => 'web'], function () {
    Route::get('/login', function () {
        return view('auth.login');
    });
    Route::post('/login', 'AuthController@login');
    Route::any('/logout', 'AuthController@logout');
    Route::any('/register', function () {
        return view('auth.register');
    });
    Route::post('/register/handle', 'AuthController@register');
    Route::any('/forgot', function () {
        return view('auth.forgot');
    });
    Route::post('/forgot/handle', 'AuthController@forgot');
    Route::any('/reset', function () {
        return view('auth.reset');
    });
    Route::post('/reset/handle', 'AuthController@reset');
});

Route::group(['prefix' => 'home', 'middleware' => ['web', 'auth']], function () {
    Route::any('/', 'HomeController@dashboard');
    Route::any('/manage', function () {
        return view('home.manage');
    });
    Route::any('/manage/list', 'HomeController@manage');
    Route::any('/manage/plugin', 'HomeController@managePlugin');
    Route::any('/manage/plugin/remove', 'HomeController@remove');

    Route::any('/add', 'HomeController@add');

    Route::any('/settings', function () {
        return view('home.settings');
    });

    Route::any('/market', function () {
        return view('home.market');
    });
    Route::any('/market/list', 'MarketController@market');
});

Route::group(['prefix' => 'user', 'middleware' => ['web', 'auth']], function () {
    Route::post('/renew/password', 'UserController@renewPassword');
    Route::post('/renew/email', 'UserController@renewEmail');
    Route::post('/renew/username', 'UserController@renewUsername');
    Route::post('/delete', 'UserController@deleteUser');
});

Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth', 'admin']], function () {
    Route::any('/', 'AdminController@dashboard');
    Route::any('/users', function () {
        return view('admin.users');
    });
    Route::any('/users/get', 'AdminController@getUser');
    Route::post('/users/post', 'AdminController@updateUser');
    Route::get('/users/list', 'AdminController@usersPage');
    Route::post('/users/delete', 'AdminController@deleteUser');

    Route::any('/plugins', function () {
        return view('admin.plugins');
    });
    Route::get('/plugins/list', 'AdminController@pluginsPage');
    Route::any('/plugins/get', 'AdminController@getPlugin');
    Route::post('/plugins/post', 'AdminController@updatePlugin');
    Route::post('/plugins/delete', 'AdminController@deletePlugin');

    Route::any('/site', function () {
        return view('admin.site');
    });
});
